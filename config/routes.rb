require 'sidekiq/web'
Rails.application.routes.draw do
  get 'dashboard/index'

  devise_for :users

  devise_scope :user do
    authenticated :user do
      # root 'projects#index', as: :authenticated_root
      root 'dashboard#index'
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  
  resources :users do
    collection { post :search, to: "users#index"}
    collection { get :search, to: "users#index"}
    get :autocomplete_tag_name, :on => :collection
    member do
      get "profile"
      post "ganti_profile"
    end
  end


  resources :projects do 
    get :dashboard, on: :member
    resources :discussions
    resources :tasks do 
      resources :todos do
        post 'completed'
        resources :discussions
      end
    end
  end

  resources :time_sheets do
    collection do
      delete 'finish' #use delete method to overide link using post data update end time working
      delete 'continue_working' #use delete method to overide link using post to delete the end working time so you can contiue working 
    end
  end
  resources :furloughs
  resources :staff_leaves
  mount Sidekiq::Web, at: '/sidekiq'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
