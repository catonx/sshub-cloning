require 'test_helper'

class NotifierTest < ActionMailer::TestCase
  test "time_sheet_submit" do
    mail = Notifier.time_sheet_submit
    assert_equal "Time sheet submit", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "time_sheet_12am" do
    mail = Notifier.time_sheet_12am
    assert_equal "Time sheet 12am", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "time_sheet_forget" do
    mail = Notifier.time_sheet_forget
    assert_equal "Time sheet forget", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
