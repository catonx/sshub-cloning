# Preview all emails at http://localhost:3000/rails/mailers/notifier
class NotifierPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notifier/time_sheet_submit
  def time_sheet_submit
    Notifier.time_sheet_submit
  end

  # Preview this email at http://localhost:3000/rails/mailers/notifier/time_sheet_12am
  def time_sheet_12am
    Notifier.time_sheet_12am
  end

  # Preview this email at http://localhost:3000/rails/mailers/notifier/time_sheet_forget
  def time_sheet_forget
    Notifier.time_sheet_forget
  end

end
