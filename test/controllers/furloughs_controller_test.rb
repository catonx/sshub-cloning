require 'test_helper'

class FurloughsControllerTest < ActionController::TestCase
  setup do
    @furlough = furloughs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:furloughs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create furlough" do
    assert_difference('Furlough.count') do
      post :create, furlough: { approved: @furlough.approved, approved_at: @furlough.approved_at, date: @furlough.date, reason: @furlough.reason, user_id: @furlough.user_id }
    end

    assert_redirected_to furlough_path(assigns(:furlough))
  end

  test "should show furlough" do
    get :show, id: @furlough
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @furlough
    assert_response :success
  end

  test "should update furlough" do
    patch :update, id: @furlough, furlough: { approved: @furlough.approved, approved_at: @furlough.approved_at, date: @furlough.date, reason: @furlough.reason, user_id: @furlough.user_id }
    assert_redirected_to furlough_path(assigns(:furlough))
  end

  test "should destroy furlough" do
    assert_difference('Furlough.count', -1) do
      delete :destroy, id: @furlough
    end

    assert_redirected_to furloughs_path
  end
end
