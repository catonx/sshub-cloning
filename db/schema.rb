# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140520111729) do

  create_table "discussions", force: true do |t|
    t.integer  "user_id"
    t.text     "content"
    t.integer  "discussrefer_id"
    t.string   "discussrefer_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "discussions", ["discussrefer_id", "discussrefer_type"], name: "index_discussions_on_discussrefer_id_and_discussrefer_type", using: :btree
  add_index "discussions", ["user_id"], name: "index_discussions_on_user_id", using: :btree

  create_table "furloughs", force: true do |t|
    t.integer  "user_id"
    t.date     "date"
    t.text     "reason"
    t.boolean  "approved"
    t.datetime "approved_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "approved_reason"
    t.string   "leave_type"
  end

  create_table "projects", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects_users", id: false, force: true do |t|
    t.integer "project_id"
    t.integer "user_id"
  end

  add_index "projects_users", ["project_id"], name: "index_projects_users_on_project_id", using: :btree
  add_index "projects_users", ["user_id"], name: "index_projects_users_on_user_id", using: :btree

  create_table "tasks", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id"
  end

  create_table "time_sheets", force: true do |t|
    t.integer  "user_id"
    t.date     "date_submitted"
    t.time     "start_time"
    t.time     "end_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "time_sheets", ["user_id"], name: "index_time_sheets_on_user_id", using: :btree

  create_table "todos", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "task_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "completed"
    t.integer  "completed_by_id"
    t.datetime "completed_at"
    t.integer  "project_id"
  end

  add_index "todos", ["completed_by_id"], name: "index_todos_on_completed_by_id", using: :btree

  create_table "todos_users", id: false, force: true do |t|
    t.integer "todo_id"
    t.integer "user_id"
  end

  add_index "todos_users", ["todo_id"], name: "index_todos_users_on_todo_id", using: :btree
  add_index "todos_users", ["user_id"], name: "index_todos_users_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "roles_mask"
    t.string   "persistence_token"
    t.string   "name"
    t.string   "username"
    t.string   "role"
    t.date     "birthdate"
    t.string   "phone_number"
    t.text     "address"
    t.string   "id_number"
    t.string   "jamsostek_number"
    t.date     "join_date"
    t.string   "npwp"
    t.integer  "manager_id"
    t.string   "avatar"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
