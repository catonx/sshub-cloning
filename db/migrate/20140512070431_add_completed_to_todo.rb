class AddCompletedToTodo < ActiveRecord::Migration
  def change
    add_column :todos, :completed, :boolean
    add_reference :todos, :completed_by, index: true
    add_column :todos, :completed_at, :datetime
  end
end
