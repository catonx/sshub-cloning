class CreateTimeSheets < ActiveRecord::Migration
  def change
    create_table :time_sheets do |t|
      t.references :user_id, index: true
      t.date :date_submitted
      t.time :start_time
      t.time :end_time

      t.timestamps
    end
  end
end
