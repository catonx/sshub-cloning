class CreateFurloughs < ActiveRecord::Migration
  def change
    create_table :furloughs do |t|
      t.integer :user_id
      t.date :date
      t.text :reason
      t.boolean :approved
      t.datetime :approved_at

      t.timestamps
    end
  end
end
