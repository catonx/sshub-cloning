class AddColumnExtendsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :roles_mask, :integer
    add_column :users, :persistence_token, :string
    add_column :users, :name, :string
    add_column :users, :username, :string
    add_column :users, :role, :string
    add_column :users, :birthdate, :date
    add_column :users, :phone_number, :string
    add_column :users, :address, :text
    add_column :users, :id_number, :string
    add_column :users, :jamsostek_number, :string
    add_column :users, :join_date, :date
    add_column :users, :npwp, :string
    add_column :users, :manager_id, :integer
    add_column :users, :avatar, :string
  end
end
