class CreateTodosUsers < ActiveRecord::Migration
  def change
    create_table :todos_users, id: false do |t|
      t.references :todo, index: true
      t.references :user, index: true
    end
  end
end
