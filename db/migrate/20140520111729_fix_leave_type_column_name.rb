class FixLeaveTypeColumnName < ActiveRecord::Migration
  def change
    rename_column :furloughs, :type, :leave_type
  end
end
