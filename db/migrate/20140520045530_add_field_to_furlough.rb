class AddFieldToFurlough < ActiveRecord::Migration
  def change
    add_column :furloughs, :approved_reason, :text
    add_column :furloughs, :type, :string
  end
end
