class CreateDiscussions < ActiveRecord::Migration
  def change
    create_table :discussions do |t|
      t.references :user, index: true
      t.text :content
      t.belongs_to :discussrefer, polymorphic: true

      t.timestamps
    end
    add_index :discussions, [:discussrefer_id, :discussrefer_type]
  end
end
