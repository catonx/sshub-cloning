namespace :notifier do
  desc "Mail Late Notification : Absence nil at 12 AM"
  task :late => :environment do
    if workdays?(Time.now)
      User.includes(:time_sheets).each do |user|
        NotifiersWorker.perform_async('time_sheet_12am', user.id, 1) unless user.submitted_today?
      end
    end
  end

  desc "Update timesheets if end_time not presence => 23:59 "
  task :go_home => :environment do
    if workdays?(Time.now)
      User.includes(:time_sheets).each do |user|
        unless user.submitted_end?
          s = TimeSheet.find_by(user_id: user.id, date_submitted: Time.now.to_date)
          s.update(end_time: "23:59:59".to_time) if s.present?
        end
      end
    end
  end

  desc "Notifier All"
  task :all => [:late]


  desc "Mail to manajer if staf leave proposal is not responded yet "
  task :seven_day => :environment do
      Furlough.all.each do |leave|
        if leave.seven_day.to_date == Time.now.to_date && leave.approved.blank? 
          NotifiersWorker.perform_async('leave_seven_day_manager_notification', leave.user_id, nil, nil, leave.id)
        end
      end
  end

  def workdays? (date)
    true unless date.saturday? || date.sunday? 
  end

  def endtime
    waktu = Time.new
    waktu.end_of_day
  end
end