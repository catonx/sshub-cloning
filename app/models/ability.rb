class Ability
  include CanCan::Ability

  def initialize(user, params=nil)
    user ||= User.new
    send(user.role, user, params) if user.role.present?
    alias_action :update,:destroy, :to => :ud
    alias_action :create, :read, :update, :destroy, :to => :crud
  end

  def junior_staff(user, params)
    can :dashboard, Project
    can :read, :all
    can :manage, Discussion
    can :create, TimeSheet
    can :profile, User
    can :update, User
    can :ganti_profile, User
    can :create, Todo do |todo|
      project = Project.find(params[:project_id])
      project.users.map(&:id).include?(user.id)
    end
    can [:update, :completed], Todo do |todo|
      todo.task.project.users.map(&:id).include?(user.id)
    end
  end

  def staff(user, params)
    junior_staff(user, params)
  end

  def senior_staff(user, params)
    staff(user, params)
    can :manage, [Todo, Task]
  end

  def manager(user, params)
    senior_staff(user, params)
    can :manage, User
    can :manage, Project
    can :manage, TimeSheet
    can :manage, Furlough
  end

  def admin(user, params)
    manager(user, params)
  end
end
