class Discussion < ActiveRecord::Base
  belongs_to :user
  belongs_to :discussrefer, polymorphic: true
  
  validates :content, presence: true
  
end
