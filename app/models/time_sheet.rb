class TimeSheet < ActiveRecord::Base
  belongs_to :user
  validates_uniqueness_of :date_submitted, scope: :user_id

  scope :today, lambda { where(date_submitted: Time.now.to_date ) }

  def format_start_time
    self.start_time.to_time.strftime('%H:%M:%S').to_time
  end

  def format_end_time
    self.end_time.to_time.strftime('%H:%M:%S').to_time
  end

  def continue_working
    #raise Time.new.to_yaml
    self.end_time = nil
    self.save!
  end

  def finish
    #raise Time.new.to_yaml
    self.end_time = Time.new.to_time.strftime('%H:%M:%S').to_time
    self.save!
  end

  
end
