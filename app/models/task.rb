class Task < ActiveRecord::Base
  validates :name, presence: true
  
  has_many :todos, dependent: :destroy
  belongs_to :project

  # has_many :discussions, as: :discussrefer
end
