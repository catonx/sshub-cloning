class Todo < ActiveRecord::Base
  validates :name, presence: true
  validate :has_users?

  belongs_to :user, class_name: "User", foreign_key: "completed_by_id"
  belongs_to :task
  belongs_to :project
  has_many :discussions, as: :discussrefer
  has_and_belongs_to_many :users

  def has_users?
    errors.add(:users, 'Please select one user') if self.users.blank?
  end

  def completed_it(uid)
    self.completed_at = Time.new.to_time.strftime('%H:%M:%S').to_time
    self.completed = true
    self.completed_by_id = uid
    self.save!
  end
end
