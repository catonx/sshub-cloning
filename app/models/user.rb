class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => [:login]
         
  mount_uploader :avatar, UserUploader

  attr_accessor :login

  validates :username,  uniqueness: { case_sensitive: false }
  validates :name, presence: true
  validates :phone_number, presence: true
  validates :email, presence: true
  validates :address, presence: true
  validates :role, presence: true
  validates :id_number, presence: true
  validates :manager_id, presence: true, confirmation: true, if: :staff?
  validates_presence_of :password, :if => :should_validate_password?
  validates_presence_of [:password,:password_confirmation], :on => :create


  has_and_belongs_to_many :projects
  has_many :staffs, class_name: "User", foreign_key: "manager_id"
  has_many :discussions
  has_many :furloughs, dependent: :destroy
  has_many :time_sheets, dependent: :destroy
  has_and_belongs_to_many :todos

  belongs_to :manager, class_name: "User"

  scope :has_staffs, lambda {where(manager_id: nil)}

  scope :with_role, lambda { |role| {:conditions => "roles_mask & #{2**ROLES.index(role.to_s)} > 0 "} }
  ROLES = %w[admin manager senior_staff staff junior_staff]

  def staff?
    role == "staff" || role == "junior_staff"
  end

  def should_validate_password?
    new_record?
  end
  # def login=(login)
  #   @login = login
  # end

  # def login
  #   @login || self.username || self.email
  # end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = lower(:value) OR lower(email) = lower(:value)", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
  

  def roles=(roles)
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.sum
  end

  def roles
    ROLES.reject { |r| ((roles_mask || 0) & 2**ROLES.index(r)).zero? }
  end

  def role?(role)
    roles.include? role.to_s
  end

  def update_profile
    @user.save
  end

  def submitted_today?
    time_sheets.today.present? && time_sheets.today.first.start_time >= "12:00".to_time
  end

  def submitted_start?
    time_sheets.today.present? && time_sheets.today.first.start_time.present?
  end
  def submitted_end?
    time_sheets.today.present? && time_sheets.today.first.end_time.present?
  end
  def self.on_board
    where('time_sheets.date_submitted = ?', Time.now.to_date ).where('time_sheets.end_time is null').select('time_sheets.*, name, role')
  end
  def self.sign_off
    where('time_sheets.date_submitted = ?', Time.now.to_date ).where('time_sheets.end_time is not null').select('time_sheets.*, name, role')
  end

  def unresolved_todos_by_project(project_id)
    count = 0
    self.todos.where(:project_id => project_id).each{|x| count = count + 1 if x.project.id == project_id &&  x.completed == nil || x.completed == false}
    count
  end

  def total_todos_by_project(project_id)
    count = 0
    self.todos.where(:project_id => project_id).each{|x| count = count + 1 if x.project.id == project_id }
    count
  end

  def resolved_todos_by_project(project_id)
    count = 0
    self.todos.where(:project_id => project_id).each{|x| count = count + 1 if x.project.id == project_id && x.completed == true || x.completed == 1}
    count
  end
end
