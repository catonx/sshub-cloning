class Project < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_many :tasks, dependent: :destroy
  has_many :todos

  validates :name, presence: true
  
  has_many :discussions, as: :discussrefer

  def resolved_by_user(user_id)
  end
end
