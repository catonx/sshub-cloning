class Furlough < ActiveRecord::Base
  validates :reason, :user_id, presence: true
  belongs_to :user

  validates_uniqueness_of :date, scope: :user_id

  def str_approve
    self.approved ? 'Accepted' : 'Rejected'
  end

  def seven_day
    created_at = self.created_at + 7.day
  end

end
