class NotifiersWorker
  include Sidekiq::Worker

  def perform(worker_id, user_id, time_sheet=nil, todo_id=nil, furlough_id= nil)
    @user = User.find(user_id)
    @manager = @user
    @time_sheet = time_sheet!=nil ? TimeSheet.find(time_sheet) : 0;
    @todo = todo_id!=nil ? Todo.find(todo_id) : 0;
    @leave = Furlough.find(furlough_id) unless furlough_id.nil? 
    case worker_id
    when 'time_sheet_submit'
      Notifier.time_sheet_submit(@user,@time_sheet).deliver
    when 'time_sheet_12am'
      Notifier.time_sheet_12am(@user,@time_sheet).deliver
    when 'time_sheet_forget'
      Notifier.time_sheet_forget(@user,@time_sheet).deliver
    when 'todo_assignation'
      Notifier.todo_assignation(@user, @todo).deliver
    when 'todo_completion'
      Notifier.todo_completion(@user, @todo).deliver
    when 'leave_manager_notification'
      Notifier.leave_manager_notification(@user, @leave).deliver
    when 'leave_manager_respond'
      Notifier.leave_manager_respond(@leave, @manager).deliver
    when 'leave_seven_day_manager_notification'
      Notifier.leave_manager_notification(@user, @leave).deliver
    else
      0
    end
  end
end