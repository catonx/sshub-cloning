class FurloughsController < ApplicationController
  before_action :set_furlough, only: [:show, :edit, :update, :destroy]

  # GET /furloughs
  # GET /furloughs.json
  def index
    # @furloughs = Furlough.all
    @search = Furlough.search(params[:q])
    @furloughs = @search.result.page(params[:page]).per(10)
    @user = current_user
  end

  # GET /furloughs/1
  # GET /furloughs/1.json
  def show
  end

  # GET /furloughs/new
  def new
    @furlough = Furlough.new
  end

  # GET /furloughs/1/edit
  def edit
  end

  # POST /furloughs
  # POST /furloughs.json
  def create
    # @furlough = Furlough.new(furlough_params)
    @furlough = current_user.furloughs.new(furlough_params)

    respond_to do |format|
      if @furlough.save
        NotifiersWorker.perform_async('leave_manager_notification', current_user.id, nil, nil, @furlough.id)
        format.html { redirect_to @furlough, notice: 'Furlough was successfully created.' }
        format.json { render :show, status: :created, location: @furlough }
      else
        format.html { render :new }
        format.json { render json: @furlough.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /furloughs/1
  # PATCH/PUT /furloughs/1.json
  def update
    @user = current_user
    if @furlough.update(furlough_params)
      if @user.role == 'manager' && @furlough.user_id != @user
        NotifiersWorker.perform_async('leave_manager_respond', @user.id, nil, nil, @furlough.id)
        redirect_to :back
      else
        redirect_to @furlough, notice: 'Furlough was successfully updated.'
      end
    else
      render :edit
    end
  end

  # DELETE /furloughs/1
  # DELETE /furloughs/1.json
  def destroy
    @furlough.destroy
    respond_to do |format|
      format.html { redirect_to furloughs_url, notice: 'Furlough was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_furlough
      @furlough = Furlough.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def furlough_params
      params.require(:furlough).permit(:user_id, :date, :reason, :approved, :approved_at, :leave_type, :approved_reason)
    end

end
