class DiscussionsController < ApplicationController
  before_action :set_discuss, only: [:show, :edit, :update, :destroy]
  before_filter :load_discussions

  def index
    #raise @discussrefer.discussions.to_yaml
    @discussions = @discussrefer.discussions
  end

  def new
    @discussion = @discussrefer.discussions.new
  end


  def create
    @discussion = @discussrefer.discussions.new(discussion_params)
    @discussion.user = current_user
    if @discussion.save
      redirect_to :back, notice: "Your comment has been saved."
    else
      render :new
    end
      
  end
  def edit
    
  end
  def destroy
    @discussion.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: "Comment has been deleted" }
      format.json { head :no_content }
    end
  end

  def update
    @discussion = Discussion.find(params[:id])
    @discussion.update_attributes(params[:discussion])
    response_to do |format|
      format.html do
        if @discussion.errors.present?
          render :edit
        else
          redirect_to polymorphic_path(@discussrefer), :notice => "Yay updated!"
          # or
          # redirect_to polymorphic_path([@commentable, @comment]), :notice => "Yay i can update and see further down the path!"
        end
      end
    end
  end

private
  def set_discuss
    @discussion = Discussion.find(params[:id])
    
  end
  def discussion_params
    params.require(:discussion).permit(:content, :project_id, :todo_id, :user_id)
  end
  def load_discussions
    # if condition
      
    # else
    #   resource, id = request.path.split('/')[1, 2]  #get argument from url /photos/5 then 1 is photos an 2 is 5
    # end
    
    # raise resource.singularize.classify.constantize.find(1).to_json
    # @discussrefer = resource.singularize.classify.constantize.find(id) 
    if params[:project_id].present? && params[:todo_id].blank?
      @discussrefer = Project.find(params[:project_id])

    elsif params[:project_id].present? && params[:todo_id].present?
      @discussrefer = Todo.find(params[:todo_id])
    end
  end

end
