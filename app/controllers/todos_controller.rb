class TodosController < ApplicationController
  before_action :set_todo, only: [:show, :edit, :update, :destroy]
  before_action :set_task
  before_action :set_project
  load_and_authorize_resource 

  # GET /todos
  # GET /todos.json
  def index
      @todos = Todo.find_by_task_id(params[:task])
  end

  # GET /todos/1
  # GET /todos/1.json
  def show
    @discussionrefer = @todo
    @discussions = @discussionrefer.discussions
    @discussion = Discussion.new
  end

  # GET /todos/new
  def new
    @todo = Todo.new
  end

  # GET /todos/1/edit
  def edit
  end

  # POST /todos
  # POST /todos.json
  def create
    @todo = @task.todos.new(todo_params)

    respond_to do |format|
      if @todo.save
        # format.html { redirect_to project_task_todos_path(@task.project_id, @task), notice: 'Todo was successfully created.' }
        # Notifier.todo_assignation(current_user, @todo).deliver
        NotifiersWorker.perform_async('todo_assignation', current_user.id, nil, @todo.id)
        format.html { redirect_to project_task_path(@task.project, @task), notice: 'Todo was successfully created.' }
        format.json { render :show, status: :created, location: @todo }
      else
        format.html { render :new }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /todos/1
  # PATCH/PUT /todos/1.json
  def update
    respond_to do |format|
      if @todo.update(todo_params)
        # Notifier.todo_assignation(current_user, @todo).deliver
        # raise current_user.to_yaml
        NotifiersWorker.perform_async('todo_assignation', current_user.id, nil, @todo.id)
        format.html { redirect_to project_task_path(@task.project, @task), notice: 'Todo was successfully updated.' }
        format.json { render :show, status: :ok, location: @todo }
      else
        format.html { render :edit }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todos/1
  # DELETE /todos/1.json
  def destroy
    @todo.destroy
    respond_to do |format|
      # format.html { redirect_to project_task_todos_path(@todo.task.project.id, @todo.task_id), notice: "Todo #{@todo.name.to_s} was successfully destroyed." }
      format.html { redirect_to project_task_path(@task.project, @task), notice: "Todo was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def completed
    @todo = Todo.find(params[:todo_id])
    if @todo.completed_it(current_user.id)
      NotifiersWorker.perform_async('todo_completion', current_user.id, nil, @todo.id)
      redirect_to :back, notice: "Thanks for complete todo"
    else
      flash[:error] = "Fail complete todo"
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo
      @todo = Todo.find(params[:id])
    end
    def set_task
      @task = Task.find(params[:task_id]) 
    end
    def set_project
      @project = Project.find(params[:project_id]) 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_params
      params.require(:todo).permit(:name, :description, :task_id, :user_ids, :project_id)
    end


    def current_ability
      @current_ability ||= Ability.new(current_user, params)
    end
end
