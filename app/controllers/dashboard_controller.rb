class DashboardController < ApplicationController
  def index
    @employees = User.count
    
    @list_employees = User.joins(:time_sheets).where('time_sheets.date_submitted = ?', Time.now.to_date ).where("time_sheets.end_time is null")
    @list_sign_off = User.joins(:time_sheets).where('time_sheets.date_submitted = ?', Time.now.to_date ).where("time_sheets.end_time is not null").where("time_sheets.start_time is not null")
    @on_board = TimeSheet.where(date_submitted: Time.now.to_date, end_time: nil ).where.not(start_time: nil).count
    @finished = TimeSheet.where(date_submitted: Time.now.to_date ).where.not(start_time: nil, end_time: nil).count
    @furlough = Furlough.where(date: Time.now.to_date ).count
    @SickLeaves = Furlough.where(date: Time.now.to_date).where(' reason like "%sakit%" ').count
    @not_arrived = @employees - @on_board - @finished
  end
end
