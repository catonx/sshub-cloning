class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy, :dashboard]
  load_and_authorize_resource

  # GET /projects
  # GET /projects.json
  def index
    # @projects_url = Project.all

    @search = Project.search(params[:q])
    @projects = @search.result.page(params[:page]).per(10)
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @project = Project.find(params[:id])
    @discussionrefer = @project
    @discussions = @discussionrefer.discussions
    @discussion = Discussion.new

    self.project_stat

    @task = Task.new

  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy 
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def dashboard
    @person = @project.users.count
    @persons = @project.users

    self.project_stat

    @discussions_temp = @todo_temp.each do |todo|
      [todo.id]
    end
    @discussion_project =  @project.discussions.where(discussrefer_type: 'Project').count
    @discussion_todos = Discussion.where(discussrefer_type: 'Todo').where(discussrefer_id: @discussions_temp)
    @discussion_last = @discussion_todos.order(created_at: :desc).last(5)
    @discussion_todo =  @discussion_todos.count
    @discussion = @discussion_todo + @discussion_project

    @todos_stat = @project.users
  end

  def project_stat
    @tasks = @project.tasks
    @task_id = @tasks.each do |task|
      [task.id]
    end
    
    @todo_temp = Todo.where(task_id: @task_id).where(project_id: @project.id)
    @todo = @todo_temp.where('completed is null').count
    @todo_completed = @todo_temp.where(completed: 1).count
    @todo_all = @todo_temp.count
    @todo_persen = @todo_completed.to_f / @todo_all.to_f * 100
    @todos = @todo_temp.where('completed is null')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, {:user_ids => []})
    end
end
