class TimeSheetsController < ApplicationController
  before_action :set_time_sheet, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  skip_authorize_resource only: [:finish, :continue_working]

  # GET /time_sheets
  # GET /time_sheets.json
  def index
    @time_sheets = TimeSheet.all
    @time_sheet = TimeSheet.new
    @furlough = Furlough.new
    @user = current_user
    # @times = Time.new
    # @time = self.workdays?(@times)
  end

  def workdays? (date)
    true unless date.saturday? || date.sunday?
  end

  # GET /time_sheets/1
  # GET /time_sheets/1.json
  def show
  
  end

  # GET /time_sheets/new
  def new
    @time_sheet = TimeSheet.new
    @user = current_user
  end

  # GET /time_sheets/1/edit
  def edit
  end

  # POST /time_sheets
  # POST /time_sheets.json
  def create
    @time_sheet = current_user.time_sheets.new(time_sheet_params)

    respond_to do |format|
      if @time_sheet.save
        NotifiersWorker.perform_async('time_sheet_submit', @time_sheet.user_id, @time_sheet.id)
        format.html { redirect_to time_sheets_path, notice: 'Time sheet was successfully created.' }
        format.json { render :show, status: :created, location: @time_sheet }
      else
        #flash[:error] = "Error !!"
        format.html { redirect_to :back, notice: 'You already start working time today !' }
        format.json { render json: @time_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /time_sheets/1
  # PATCH/PUT /time_sheets/1.json
  def update
    respond_to do |format|
      if @time_sheet.update(time_sheet_params)
        format.html { redirect_to @time_sheet, notice: 'Time sheet was successfully updated.' }
        format.json { render :show, status: :ok, location: @time_sheet }
      else
        format.html { render :edit }
        format.json { render json: @time_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_sheets/1
  # DELETE /time_sheets/1.json
  def destroy
    @time_sheet.destroy
    respond_to do |format|
      format.html { redirect_to time_sheets_url, notice: 'Time sheet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def finish
    if current_user.time_sheets.today.last.finish
      redirect_to :back, notice: "Thanks for working today"
    else
      flash[:error] = "Error!!"
      redirect_to :back
    end
  end

  def continue_working
    if current_user.time_sheets.today.last.continue_working
      redirect_to :back, notice: "Welcome back"
    else
      flash[:error] = "Error!!"
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_time_sheet
      @time_sheet = TimeSheet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def time_sheet_params
      params.require(:time_sheet).permit(:user_id, :date_submitted, :start_time, :end_time)
    end
end
