class StaffLeavesController < ApplicationController
  def index
    @search = Furlough.search(params[:q])
    @manager = User.find(current_user)
    @staffs =  @manager.staffs
    @furloughs = Furlough.where(:user_id => @staffs)
    
  end
end
