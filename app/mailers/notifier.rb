class Notifier < ActionMailer::Base
  default from: "Software Seni Hub <hub@softwareseni.com>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.time_sheet_submit.subject
  #
  def time_sheet_submit(user_id, time_sheet_id)
    @user = User.find(user_id)
    @time_sheet = TimeSheet.find(time_sheet_id)
    @greeting = "Hi"
    mail(:to => "#{@user.email}", :subject => "Time Sheet Notification." )
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.time_sheet_12am.subject
  #
  def time_sheet_12am(user_id, time_sheet_id)
    @user = User.find(user_id)
    # @time_sheet = TimeSheet.find(time_sheet_id)
    @greeting = "Hi"
    mail(:to => "#{@user.email}", :subject => "Time Sheet Notification." )
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.time_sheet_forget.subject
  #
  def time_sheet_forget(user, time_sheet_id)
    @user = User.find(user_id)
    @time_sheet = TimeSheet.find(time_sheet_id)    
    @greeting = "Hi"
    mail(:to => "#{@user.email}", :subject => "Time Sheet Notification." )
  end

  def leave_manager_notification(user, furlough)
    @furlough = furlough
    @user = user
    @manager = user.manager
    #raise @manager.name.to_yaml
    @greeting = "Hi #{@manager.name} "
    mail(:to => "#{@manager.email}", :subject => "Leave Notification." )
  end

  def todo_assignation(user, todo)
    @user_t = todo.users.map(&:name).join(', ')
    @mail_t = todo.users.map(&:email).join(', ')
    @user_s = user.name
    @todo = todo
    @task = todo.task
    @project = @task.project
    mail(:to => @mail_t, :subject => "Todo Assignation." )
  end
  def todo_completion(user, todo)
    @user_s = user.name
    @todo = todo
    @task = todo.task
    @project = @task.project
    @mail_t = @project.users.map(&:email).join(', ')
    mail(:to => @mail_t, :subject => "Todo Completion." )
  end
  def leave_manager_respond(furlough, manager)
    @furlough = furlough
    @staff = User.find(@furlough.user_id)
    @manager = manager
    @greeting = "Hi #{@staff.name} "
    mail(:to => "#{@staff.email}", :subject => "Leave Proposal Responded" )
  end
  def leave_seven_day_manager_notification(user, furlough)
    @furlough = furlough
    @user = user
    @manager = user.manager
    #raise @manager.name.to_yaml
    @greeting = "Hi #{@manager.name} "
    mail(:to => "#{@manager.email}", :subject => "Leave Notification." )
  end
end
