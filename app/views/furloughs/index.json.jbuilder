json.array!(@furloughs) do |furlough|
  json.extract! furlough, :id, :user_id, :date, :reason, :approved, :approved_at
  json.url furlough_url(furlough, format: :json)
end
