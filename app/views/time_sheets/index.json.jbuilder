json.array!(@time_sheets) do |time_sheet|
  json.extract! time_sheet, :id, :user_id_id, :date_submitted, :start_time, :end_time
  json.url time_sheet_url(time_sheet, format: :json)
end
