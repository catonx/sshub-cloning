json.array!(@users) do |user|
  json.extract! user, :id, :email, :name, :username, :role, :birthdate, :phone_number, :address, :id_number, :jamsostek_number, :join_date, :npwp, :manager_id
  json.url user_url(user, format: :json)
end
