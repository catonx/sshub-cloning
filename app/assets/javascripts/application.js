// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require js/jquery-1.8.3.min
//= require jquery_ujs
//= require jquery-slimscroll/jquery-ui-1.9.2.custom.min
//= require jquery-slimscroll/jquery.slimscroll.min
//= require bootstrap/js/bootstrap.min
//= require bootstrap-datepicker/js/bootstrap-datepicker
//= require js/jquery.blockui
//= require js/jquery.cookie
//= require js/select2
//= require jquery-knob/js/jquery.knob
//= require flot/jquery.flot
//= require flot/jquery.flot.resize
//= require js/jquery.peity.min
//= require uniform/jquery.uniform.min
//= require datepicker
//= require nprogress
//= require ajax.spin
//= require js/scripts
//= require turbolinks
